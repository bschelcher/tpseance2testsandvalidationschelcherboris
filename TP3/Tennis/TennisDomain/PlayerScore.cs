﻿namespace TennisDomain
{
    public class PlayerScore
    {
        public readonly List<int> possibleScores = new List<int>{ 0, 15, 30, 40};

        private int score = 0;
        public int Score
        {
            get { return score; }
            private set { score = value; }
        }

        private int games = 0;
        public int Games
        {
            get { return games; }
            private set { games = value; }
        }

        public void IncreaseScore()
        {
            int newScoreIndex = possibleScores.FindIndex(possibleScore => possibleScore.Equals(score)) + 1;
            int newScore = possibleScores.ElementAt(newScoreIndex);
            Score = newScore;
        }

        public void IncreaseGames()
        {
            Games++;
        }

        public void ResetScore()
        {
            Score = possibleScores.First();
        }

        public bool IsMaximumScore() => Score == possibleScores.Last();
    }
}