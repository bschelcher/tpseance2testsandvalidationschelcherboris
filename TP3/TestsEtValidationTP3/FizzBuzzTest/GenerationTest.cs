using FizzBuzzDomain;
using Xunit;

namespace FizzBuzzTest
{
    public class GenerationTest
    {
        private Generator generator;

        public GenerationTest()
        {
            generator = new Generator();
        }

        [Fact]
        public void Generate3_Return12Fizz()
        {
            string expectedResult = "1 2 Fizz ";
            var generatedResult = generator.Generate(3);
            Assert.Equal(expectedResult, generatedResult);
        }

        [Fact]
        public void Generate5_Return12Fizz4Buzz()
        {
            string expectedResult = "1 2 Fizz 4 Buzz ";
            var generatedResult = generator.Generate(5);
            Assert.Equal(expectedResult, generatedResult);
        }

        [Fact]
        public void Generate5_12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz()
        {
            string expectedResult = "1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz ";
            var generatedResult = generator.Generate(15);
            Assert.Equal(expectedResult, generatedResult);
        }

        [Fact]
        public void GenerateTwiceWithSameGenerator_CleanBeforeGeneration()
        {
            string expectedResult = "1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz ";
            generator.Generate(8);
            var generatedResult = generator.Generate(15);
            Assert.Equal(expectedResult, generatedResult);
        }
    }
}