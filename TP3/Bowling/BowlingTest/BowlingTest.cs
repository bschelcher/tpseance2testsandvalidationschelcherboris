using Xunit;
using BowlingDomain;

namespace BowlingTest
{
    public class BowlingTest
    {
        [Fact]
        public void ScoreIsEqualToZeroWhenAllThrowIsFailled()
        {
            Bowling bowling = new Bowling();
            int[] gameThrowList = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            int expectedScore = 0;
            
            for(int i = 0; i < 20; i++)
            {
                bowling.Play(gameThrowList[i]);
            }
            int resultScore = bowling.Score();
            Assert.Equal(expectedScore, resultScore);
        }

        [Fact]
        public void ScoreWithRandomValueWithoutSpareAndStrike()
        {
            Bowling bowling = new Bowling();
            int[] gameThrowList = new int[] { 2, 4, 5, 2, 0, 4, 6, 0, 8, 1, 9, 0, 3, 3, 1, 1, 4, 4, 0, 2 };
            int expectedScore = 59;

            for (int i = 0; i < 20; i++)
            {
                bowling.Play(gameThrowList[i]);
            }
            int resultScore = bowling.Score();
            Assert.Equal(expectedScore, resultScore);
        }

        [Fact]
        public void ScoreWithPerfect()
        {
            Bowling bowling = new Bowling();
            int[] gameThrowList = new int[] { 10, 0, 10, 0,10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0};
            int expectedScore = 300;

            for (int i = 0; i < 20; i++)
            {
                bowling.Play(gameThrowList[i]);
            }
            int resultScore = bowling.Score();
            Assert.Equal(expectedScore, resultScore);
        }

        [Fact]
        public void ScoreWithRandomValueAndALessThanTenStrike()
        {
            Bowling bowling = new Bowling();
            int[] gameThrowList = new int[] { 2, 4, 10, 0, 5, 4, 6, 0, 8, 1, 10, 0, 3, 3, 1, 1, 4, 4, 10, 0 }; ;
            int expectedScore = 96;

            for (int i = 0; i < 20; i++)
            {
                bowling.Play(gameThrowList[i]);
            }
            int resultScore = bowling.Score();
            Assert.Equal(expectedScore, resultScore);
        }








    }
}