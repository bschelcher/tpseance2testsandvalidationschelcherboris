﻿namespace BowlingDomain;
public class Bowling
{
    private int[] result = new int[20];
    private int score = 0;
    private int index = 0;

    public void Play(int throwScore)
    {
        addingToResult(throwScore);

    }

    public int Score()
    {
        for (int i = 0; i < 20; i++)
        {

            if (result[i] == 10)
            {
                score += result[i] + 2 * result[(i + 1) % 20] + 2 * result[(i + 2) % 20];

            }
            else
            {
                score += result[i];
            }

        }
        return score;
    }

    private void addingToResult(int value)
    {
            result[index] = value;
            index++;
    }


}

