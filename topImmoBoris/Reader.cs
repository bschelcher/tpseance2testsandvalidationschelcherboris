﻿using System;
namespace topImmoBoris
{
    public class Reader
    {
        private string stringInput;
        private int intInput;
        private uint uintInput;
        public Reader()
        {
        }

        public string ReadString()
        {
            do
            {
                this.stringInput = Console.ReadLine();
            } while (this.stringInput == "");
            Console.Write("\n");
            return stringInput;
        }

        public int ReadInt()
        {
            var dataRead = "";
            do
            {
                dataRead = Console.ReadLine();
            } while (int.TryParse(dataRead, out this.intInput) != true);
            Console.Write("\n");
            return intInput;
        }

        public uint ReadUint()
        {
            var dataRead = "";
            do
            {
                dataRead = Console.ReadLine();
            } while (uint.TryParse(dataRead, out this.uintInput) != true);
            Console.Write("\n");
            return uintInput;
        }

        public bool ReadYorN()
        {
            string dataRead = "";
            do
            {
                dataRead = Console.ReadLine();
            } while (!dataRead.Equals("y") & !dataRead.Equals("n"));
            Console.Write("\n");
            return dataRead == "y";
        }
    }
}
