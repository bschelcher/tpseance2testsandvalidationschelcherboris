﻿using System;
namespace topImmoBoris
{
    public class ConsoleLoan
    {
        Printer printer = new Printer();
        Reader reader = new Reader();
        Assurance assurance = new Assurance();

        private uint _LoanTimeWanted;
        private uint _LoanPriceWanted;

        private const uint _MinLoanPrice = 50000;
        private const uint _MinLoanDuration = 9;
        private const uint _MaxLoanDuration = 25;

        public ConsoleLoan()
        {

            printer.Print("Welcome to the RealEstateLoan App");
            askInforLoanAssurance();
            printer.Print(assurance.GetAssuranceRate().ToString());
            askForNumberYearsWantedForLoan();
            askForLoanPriceWanted();
            RealEstateLoan realEstateLoan = new RealEstateLoan(this._LoanPriceWanted, this._LoanTimeWanted, assurance.GetAssuranceRate());

            printer.Print("total mensuality");
            printer.Print(realEstateLoan.geteMensualityTotal().ToString());

            printer.Print("assurance mensuality");
            printer.Print(realEstateLoan.getMensualityAssurance().ToString());


            printer.Print("total loan price");
            printer.Print(realEstateLoan.getTotalRatePrice().ToString());


            printer.Print("total assurance price");
            printer.Print(realEstateLoan.getTotalAssuranceRatePrice().ToString());


            uint duration;
            do
            {
                printer.Print("Price after x time, enter a value ");
                duration = reader.ReadUint();
            } while (duration < _MinLoanDuration || duration > this._LoanTimeWanted);

            printer.Print("You will have paid after" + duration + "years");
            printer.Print(realEstateLoan.getHowMuchhasbeenPaidInYears(duration,realEstateLoan.getTotalRate()).ToString());

        }

        private void askForLoanPriceWanted()
        {
            do
            {
                printer.Print("Wich amount need you ? (The minimum is 50 000) ");
                this._LoanPriceWanted = reader.ReadUint();
            } while (this._LoanPriceWanted < _MinLoanPrice);
        }

        private void askForNumberYearsWantedForLoan()
        {
            do
            {
                printer.Print("Wich duration need you ? (Min 9 years and Max 25 years) ");
                this._LoanTimeWanted = reader.ReadUint();
            } while (this._LoanTimeWanted < _MinLoanDuration || this._LoanTimeWanted > _MaxLoanDuration);
        }

        private void askInforLoanAssurance()
        {
            askIsAtheltic();
            askIsSmoker();
            askIsHeartTrouble();
            askIsComputeEngineer();
            askIsFighterPilot();
        }

        private void askIsComputeEngineer()
        {
            printer.Print("Are you Computer Engineer ? y or n");
            assurance.IsComputerEngineer(reader.ReadYorN());
        }

        private void askIsAtheltic()
        {
            printer.Print("Are you Athetic ? y or n");
            assurance.IsAthletic(reader.ReadYorN());
        }

        private void askIsSmoker()
        {
            printer.Print("Do you Smoke ? y or n");
            assurance.IsSmoker(reader.ReadYorN());
        }

        private void askIsFighterPilot()
        {
            printer.Print("Are you Fighter Pilot ? y or n");
            assurance.IsFighterPilot(reader.ReadYorN());
        }

        private void askIsHeartTrouble()
        {
            printer.Print("Have you Heart Trouble ? y or n");
            assurance.HasHeartTrouble(reader.ReadYorN());
        }


    }
}
