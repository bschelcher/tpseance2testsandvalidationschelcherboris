﻿using System;
namespace topImmoBoris
{
    public class Assurance
    {

        private double _AssuranceRate;

        private const double _AssuranceRateBase = 0.003;
        private const double _AssuranceRateBonusComputerEngineer = -0.0005;
        private const double _AssuranceRateMalusFighterPilot = 0.0015;
        private const double _AssuranceRateBonusAthletic = -0.0005;
        private const double _AssuranceRateMalusHeartTrouble = 0.003;
        private const double _AssuranceRateMalusSmoker = 0.0015;

        public Assurance()
        {
            init();
        }

        private void init()
        {
            this._AssuranceRate += _AssuranceRateBase;
        }

        public void IsComputerEngineer(bool answer)
        {
            if(answer != false)this._AssuranceRate += _AssuranceRateBonusComputerEngineer;
        }

        public void IsFighterPilot(bool answer)
        {
            if (answer != false) this._AssuranceRate += _AssuranceRateMalusFighterPilot;
        }

        public void IsAthletic(bool answer)
        {
            if (answer != false) this._AssuranceRate += _AssuranceRateBonusAthletic;
        }

        public void IsSmoker(bool answer)
        {
            if (answer != false) this._AssuranceRate += _AssuranceRateMalusSmoker;
        }

        public void HasHeartTrouble(bool answer)
        {
            if (answer != false) this._AssuranceRate += _AssuranceRateMalusHeartTrouble;
        }

        public double GetAssuranceRate()
        {
            return this._AssuranceRate;
        }
    }
}
