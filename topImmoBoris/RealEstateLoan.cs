﻿using System;
namespace topImmoBoris
{
    public class RealEstateLoan
    {
        private uint _loanAmount;
        private double _loanRateWithoutAssurance;
        private double _AssuranceRate;
        private uint _loanDurationInYear;


        private const uint _monthInAYear = 12;

        public RealEstateLoan(uint loanPrice, uint loanDuration, double assuranceRate)
        {
            this._loanDurationInYear = loanDuration;
            this._loanAmount = loanPrice;
            this._AssuranceRate = assuranceRate;
            this._loanRateWithoutAssurance = calculateLoanRateWithoutAssurance();

        }



        public double calculateLoanRateWithoutAssurance()
        {
            double valueToReturn = 0 ;
            switch (this._loanDurationInYear)
            {
                case 10:
                    valueToReturn = 0.62;
                    break;

                case 15:
                    valueToReturn = 0.67;
                    break;

                case 20:
                   valueToReturn = 0.85;
                    break;

                case 24:
                    valueToReturn = 1.04;
                    break;

                case 25:
                    valueToReturn = 1.27;
                    break;
            }
            return valueToReturn;
        }


        public double getHowMuchhasbeenPaidInYears(uint years, double rate)
        {
            double mensuality = mensualityCalcul(rate);
            return _monthInAYear * years * mensuality;
        }

        public double getTotalRatePriceWithoutAssurance()
        {
            return _loanRateWithoutAssurance * _loanAmount;
        }

        public double getTotalAssuranceRatePrice()
        {
            return _AssuranceRate * _loanAmount;
        }

        public double getTotalRatePrice()
        {
            return getTotalRate() * _loanAmount;
        }

        public double mensualityCalcul(double rate)
        {
            return (this._loanAmount * (rate / _monthInAYear)) / Math.Pow((double)1 - (1 + (rate / _monthInAYear)), (double)-(_loanDurationInYear *_monthInAYear));

        }

        public double geteMensualityTotal()
        {
            return mensualityCalcul(getTotalRate());

        }

        public double getMensualityLoanWithoutAssurance()
        {
            return mensualityCalcul(this._loanRateWithoutAssurance);
        }

        public double getMensualityAssurance()
        {
            return mensualityCalcul(this._AssuranceRate);
        }

       public double getTotalRate()
        {
            return this._AssuranceRate + this._loanRateWithoutAssurance;
        }
    }
}
